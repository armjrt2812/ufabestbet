// @ts-nocheck
import Vue from "vue";
import Router from "vue-router";
import store from "./store";
Vue.use(Router);

const onlyUser = (to, from, next) => {
  if (
    typeof store.state.token === "undefined" ||
    store.state.token === null ||
    store.state.token === "" ||
    store.state.user === null
  ) {
    next("/");
  } else {
    next();
  }
};

export default new Router({
  mode: "history",
  routes: [
    { path: "*", component: () => import("@/views/ErrorScreen") },
    {
      path: "/",
      redirect: "/home",
    },
    {
      path: "/home",
      name: "Home",
      component: () => import("@/views/Home"),
    },
    {
      path: "/article/detail/:id",
      name: "article detail",
      component: () => import("@/views/ArticleDetail"),
    },
    {
      path: "/promotion",
      name: "promotion",
      component: () => import("@/views/Promotion"),
    },
    {
      path: "/contact-us",
      name: "contact us",
      component: () => import("@/views/ContactUs"),
    },
    {
      path: "/admin-login",
      name: "Admin login",
      component: () => import("@/views/AdminLogin"),
    },
    {
      path: "/transaction",
      name: "Manage transaction",
      hidden: false,
      beforeEnter: onlyUser,
      meta: {
        title: "จัดการเดินบัญชี",
        icon: "dollar-sign",
        breadcrumb: [{ title: "จัดการเดินบัญชี", path: "/transaction" }],
      },
      component: () => import("@/views/ManageTransaction"),
    },
    {
      path: "/article",
      name: "Manage article",
      hidden: false,
      beforeEnter: onlyUser,
      meta: {
        title: "จัดการบทความ",
        icon: "documentation",
        breadcrumb: [{ title: "จัดการบทความ", path: "/transaction" }],
      },
      component: () => import("@/views/ManageArticle"),
    },
    {
      path: "/add/article",
      name: "Add article",
      hidden: true,
      beforeEnter: onlyUser,
      meta: {
        title: "เพิ่มบทความ",
        icon: "image",
        breadcrumb: [{ title: "เพิ่มบทความ", path: "/article" }],
      },
      component: () => import("@/views/AddArticle"),
    },
    {
      path: "/edit/article/:id",
      name: "edit article",
      hidden: true,
      beforeEnter: onlyUser,
      meta: {
        title: "แก้ไขบทความ",
        icon: "image",
        breadcrumb: [{ title: "แก้ไขบทความ", path: "/article" }],
      },
      component: () => import("@/views/AddArticle"),
    },
    {
      path: "/banner",
      name: "Manage banner",
      hidden: false,
      beforeEnter: onlyUser,
      meta: {
        title: "จัดการแบนเนอร์",
        icon: "image",
        breadcrumb: [{ title: "จัดการแบนเนอร์", path: "/banner" }],
      },
      component: () => import("@/views/ManageBanner"),
    },
    {
      path: "/add/banner",
      name: "Add banner",
      hidden: true,
      beforeEnter: onlyUser,
      meta: {
        title: "เพิ่มแบนเนอร์",
        icon: "image",
        breadcrumb: [{ title: "เพิ่มแบนเนอร์", path: "/banner" }],
      },
      component: () => import("@/views/Addbanner"),
    },
    {
      path: "/edit/banner/:id",
      name: "edit banner",
      hidden: true,
      beforeEnter: onlyUser,
      meta: {
        title: "แก้ไขแบนเนอร์",
        icon: "image",
        breadcrumb: [{ title: "แก้ไขแบนเนอร์", path: "/banner" }],
      },
      component: () => import("@/views/Addbanner"),
    },
    {
      path: "/users",
      name: "Manage users",
      hidden: false,
      beforeEnter: onlyUser,
      meta: {
        title: "จัดการบัญชีผู้ใช้",
        icon: "user",
        breadcrumb: [{ title: "จัดการบัญชีผู้ใช้", path: "/users" }],
      },
      component: () => import("@/views/ManageUser"),
    },
  ],
});
