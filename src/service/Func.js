export default {
  methods: {
    getImg(img) {
      if (typeof img != "undefined") {
        let url = process.env.VUE_APP_IMAGE_URL_BASE;
        let line = img.slice(2, img.length);
        return url + line;
      } else {
        return "";
      }
    },
  },
};
