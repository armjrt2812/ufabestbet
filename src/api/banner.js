import { HTTP } from "@/service/axios";
export default {
    getAllBanner: async (obj,token) =>
    {
         HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
        let res = await HTTP.post( `/banner/all`,obj ).catch( e =>
        {
            return {
                data: {
                    data: e,
                    success: false
                }
            }
        })
        return res 
    },
    DeleteBanner: async (obj,token) =>
    {
         HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
        let res = await HTTP.post( `/banner/delete`,obj ).catch( e =>
        {
            return {
                data: {
                    data: e,
                    success: false
                }
            }
        })
        return res 
    },
  
}