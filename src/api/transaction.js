import { HTTP } from "@/service/axios";
export default {
  GetAll: async (obj, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
    let res = await HTTP.post(`/user/all/payment/transaction`, obj).catch(
      (e) => {
        return {
          data: {
            data: e,
            success: false,
          },
        };
      }
    );
    return res;
  },
};
