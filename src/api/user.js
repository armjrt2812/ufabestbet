import { HTTP } from "@/service/axios";
export default {
  register: async (obj) => {
    let res = await HTTP.post(`/user/register`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  checkLine: async (obj) => {
    let res = await HTTP.post(`/user/check/line`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  checkMobile: async (obj) => {
    let res = await HTTP.post(`/user/check/mobile`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  checkAccount: async (obj) => {
    let res = await HTTP.post(`/user/check/account`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  adminSignIn: async (obj) => {
    let res = await HTTP.post(`/user/admin/sign/in`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  getAllUser: async (obj, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;

    let res = await HTTP.post(`/user/search`, obj).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
};
