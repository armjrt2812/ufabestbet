import { HTTP } from "@/service/axios";
export default {
  getAll: async (obj, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;

    let res = await HTTP.get(
      `/article/all?skip=${obj.skip}&limit=${obj.limit}`
    ).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  GetById: async (id, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
    let res = await HTTP.get(`/article/by?id=${id}`).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  UpdateToPromotion: async (id, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
    let res = await HTTP.put(`/article/to/promotion?id=${id}`).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
  Delete: async (id, token) => {
    HTTP.defaults.headers.common.Authorization = `Bearer ${token}`;
    let res = await HTTP.delete(`/article/remove?_id=${id}`).catch((e) => {
      return {
        data: {
          data: e,
          success: false,
        },
      };
    });
    return res;
  },
};
