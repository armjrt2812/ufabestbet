import { HTTP } from "@/service/axios";

export default {
  FETCH_TEST: async ({ commit }, obj) => {
    commit("SET_TEST", obj)
  },
};
