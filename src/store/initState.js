import test from "./constant/test";
import Cookies from "js-cookie";

const initState = () => ({
  test: test,
  sidebar: {
    opened: Cookies.get("sidebarStatus")
      ? !!+Cookies.get("sidebarStatus")
      : false,
    withoutAnimation: false,
  },
  device: "desktop",
  showSettings: null,
  fixedHeader: false,
  sidebarLogo: false,
  user: null,
  token: "",
  referForm: [
    {
      label: "กลุ่ม Facebook",
    },
    {
      label: "Line",
    },
    {
      label: "เพื่อนแนะนำ",
    },
    {
      label: "แอปพลิเคชั่น",
    },
    {
      label: "อื่นๆ",
    },
  ],
  bankName: [
    { name: "ธนาคารกสิกรไทย (KBANK)", value: "ธนาคารกสิกรไทย" },
    { name: "ธนาคารไทยพาณิชย์ (SCB)", value: "ธนาคารไทยพาณิชย์" },
    { name: "ธนาคารกรุงไทย (KTB)", value: "ธนาคารกรุงไทย" },
    { name: "ธนาคารกรุงเทพ (BBL)", value: "ธนาคารกรุงเทพ" },
    { name: "ธนาคารกรุงศรีอยุธยา (BAY)", value: "ธนาคารกรุงศรีอยุธยา" },
    { name: "ธนาคารออมสิน (GSB)", value: "ธนาคารออมสิน" },
    { name: "ธนาคารทหารไทยธนชาต (TTB)", value: "ธนาคารทหารไทยธนชาต" },
    { name: "ธนาคารเกียรตินาคินภัทร (KKP)", value: "ธนาคารเกียรตินาคินภัทร" },
    { name: "ธนาคารซีไอเอ็มบีไทย (CIMBT)", value: "ธนาคารซีไอเอ็มบีไทย" },
    { name: "ธนาคารทิสโก้ (TISCO)", value: "ธนาคารทิสโก้" },
    { name: "ธนาคารยูโอบี (UOBT)", value: "ธนาคารยูโอบี" },
    { name: "ธนาคารอาคารสงเคราะห์ (GHB)", value: "ธนาคารอาคารสงเคราะห์" },
    {
      name: "ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร (BAAC)",
      value: "ธนาคารเพื่อการเกษตรและสหกรณ์การเกษตร",
    },
  ],
});

export default initState;
