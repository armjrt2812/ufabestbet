import Cookies from "js-cookie";
import initState from "./initState";
export default {
  TOGGLE_SIDEBAR: (state) => {
    let obj = {
      opened: !state.sidebar.opened,
      withoutAnimation: state.sidebar.withoutAnimation,
    };
    state.sidebar = obj;

    if (state.sidebar.opened) {
      Cookies.set("sidebarStatus", 1);
    } else {
      Cookies.set("sidebarStatus", 0);
    }
  },
  SIGNOUT: (state) => {
    const initial = initState();
    Object.keys(initial).forEach((key) => {
      state[key] = initial[key];
    });
  },
  CLOSE_SIDEBAR: (state, payload) => {
    Cookies.set("sidebarStatus", 0);
    state.sidebar.opened = false;
    state.sidebar.withoutAnimation = payload;
  },
  SET_USER: (state, payload) => {
    state.user = payload;
  },
  SET_TOKEN: (state, payload) => {
    state.token = payload;
  },
  TOGGLE_DEVICE: (state, payload) => {
    state.device = payload;
  },
};
