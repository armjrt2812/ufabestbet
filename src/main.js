import Vue from "vue";
import ElementUI from "element-ui";
import "element-ui/lib/theme-chalk/index.css";

import "normalize.css/normalize.css";

import "@/styles/index.scss";
import "animate.css";
import "@/icons";

import App from "./App.vue";
import router from "./router";
import store from "./store";
import locale from "element-ui/lib/locale/lang/en";
import moment from "moment";
import momentTH from "moment/src/locale/th";
import VueMask from "v-mask";
import Func from "@/service/Func";
import VueQuillEditor from "vue-quill-editor";

// require styles
import "quill/dist/quill.core.css";
import "quill/dist/quill.snow.css";
import "quill/dist/quill.bubble.css";
Vue.use(VueQuillEditor /* { default global options } */);
Vue.use(VueMask);
Vue.mixin(Func);
Vue.filter("dateFomatNotTime", function (date) {
  let res = "";
  if (typeof date === "undefined" || date === null || date === "") {
    res = "-";
  } else {
    res = moment(moment(date).locale("th", momentTH).add(543, "y")).format(
      "DD MMMM Y"
    );
  }
  return res;
});

Vue.filter("dateTimeTh", (date) => {
  let res = "";
  if (typeof date === "undefined" || date === null || date === "") {
    res = "-";
  } else {
    res = moment(moment(date).locale("th", momentTH).add(543, "y")).format(
      "lll น."
    );
  }
  return res;
});

Vue.config.productionTip = false;
Vue.use(ElementUI, { locale });

new Vue({
  store,
  router,
  render: (h) => h(App),
}).$mount("#app");
